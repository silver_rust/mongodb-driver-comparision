import { liftDB } from "./db"
import { insert_single_linear, insert_single_parallel, insert_bulk } from "./tests";


main().catch()
async function main (){
    let {db, client} = await liftDB();

    console.log("Node.js")
    console.log("Times in ms")

    await insert_single_linear(db);
    await insert_single_parallel(db);
    await insert_bulk(db);

    client.close();
}

