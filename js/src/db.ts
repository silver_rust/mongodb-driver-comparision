import dotenv from 'dotenv'
import {MongoClient, Db, Client} from "mongodb"

// exporting db types


dotenv.config();

const dbLocation = process.env.dbLocation;
const dbUser = process.env.dbUser;
const dbPass = process.env.dbPass;
const database = process.env.db;
const dbURL = 'mongodb+srv://' + dbUser + ':' + dbPass + '@' + dbLocation + '/admin?ssl=false';

export {Client, Db}
export interface LiftDb {
    db: Db,
    client: Client
}
interface UpdateOne<T>{
    updateOne: {
        filter: {
            [propName: string]: any
        }
        update: {
            $set: Partial<T>
            $setOnInsert?: Partial<T>
        }
        upsert: boolean
    }
}
interface DataToDbArray<T>{
    arrays: number
    [propName: number] : UpdateOne<T>[]
}
export async function liftDB (): Promise<LiftDb>{
    let client: Client = await MongoClient.connect(dbURL, { useNewUrlParser: true, useUnifiedTopology: true }).catch((err) => {console.log("MongoDB failed to connect, exiting"); console.log(err); process.exit(1) });
    let db: Db = client.db(database);

    // create base collection and index
    await db.createCollection("test").catch(()=>{});
    await db.collection('test').createIndex({ 'id': 1 }).catch(()=>{});

    return {db, client}
}


export async function addItem<T>(db: Db, collection:string, item:Partial<T>, field:string = "id"){
    if(!collection){return}
    let find = {
        [field]: item[field]
    }
    return await db.collection(collection).updateOne(find, {$set: item}, {upsert: true})
}

export async function addItemBulk<T> (db: Db, collection: string,  dbArray: DataToDbArray<T>, location:Error = new Error()){
    if(!collection){return}
    for (let i = 0; i < dbArray.arrays + 1; i++) {
        if (dbArray[i].length > 0) {
            await db.collection(collection).bulkWrite(dbArray[i], { ordered: false }).catch((err) => console.log(collection, err,location))
        }
    }
}

export function dataToDbArray<T> (data:Array<Partial<T>>, limit:number= 25000, accessor:string = "id", setOnInsert?:Partial<T>): DataToDbArray<T> {
    let tmp: DataToDbArray<T> = { arrays: 0, 0: [] };
    for (let j = 0; j < data.length; j++) {
        let temp: UpdateOne<T> = {
            updateOne: {
                filter:{
                    [accessor]: data[j][accessor]
                },
                update: {
                    $set: data[j]
                },
                upsert: true
            }
        };

        if (setOnInsert) {
            temp.updateOne.update["$setOnInsert"] = setOnInsert
        }
        if (tmp[tmp.arrays].length < limit) {
            tmp[tmp.arrays].push(temp)
        } else {
            tmp.arrays += 1;
            tmp[tmp.arrays] = [temp]
        }
    }
    return tmp
}
