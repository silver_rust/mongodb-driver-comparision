import { addItem, addItemBulk, dataToDbArray, Db } from "./db"


interface TestObject {
    id: number
    field1: string
    field2: string
}

function generate_array():TestObject[]{
    let data:TestObject[] = [];

    while (data.length < 1000){
        data.push({id:data.length, field1: "", field2: ""})
    }

    return data
}



export async function insert_single_linear(db: Db){
    let items = generate_array();

    // time how long it takes
    let startTimer = Date.now();

    for (const item of items) {
        await addItem<TestObject>(db, "test", item, "id");
    }

    let elapsed = (Date.now() - startTimer);

    console.log(`insert_single_linear - items: ${items.length}  Time: ${elapsed}  Each: ${elapsed/items.length}`)
}

export async function insert_single_parallel(db: Db){
    let items = generate_array();

    // time how long it takes
    let startTimer = Date.now();

    let promises = items.map(async (item) => {
        await addItem<TestObject>(db, "test", item, "id")
    })
    await Promise.all(promises).catch(err => console.log(err));

    let elapsed = (Date.now() - startTimer);

    console.log(`insert_single_parallel - items: ${items.length}  Time: ${elapsed}  Each: ${elapsed/items.length}`)
}

export async function insert_bulk(db: Db){
    let items = generate_array();

    // time how long it takes
    let startTimer = Date.now();

    let toDBArray = dataToDbArray<TestObject>(items);

    await addItemBulk<TestObject>(db, "test", toDBArray)

    let elapsed = (Date.now() - startTimer);

    console.log(`insert_bulk - items: ${items.length}  Time: ${elapsed}  Each: ${elapsed/items.length}`)
}