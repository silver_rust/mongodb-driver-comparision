# What
The purporse of this is to test the update preformance of different mongodb crates while also serving to advance my own rust skills.


# Results
These are the results of the tests on my local machiene while conencted to my own db


```
Node.js
Times in ms
insert_single_linear - items: 1000  Time: 42324  Each: 42.324
insert_single_parallel - items: 1000  Time: 4435  Each: 4.435
insert_bulk - items: 1000  Time: 108  Each: 0.108


Rust
Times in ms
insert_single_linear - Stable - items: 10000  Time: 463091  Each: 46.3091
insert_single_linear - Alpha - items: 10000  Time: 464644  Each: 46.4644
insert_single_linear - Git - items: 10000  Time: 519713  Each: 51.9713
insert_single_parallel - Stable - items: 10000  Time: 25930  Each: 2.593
insert_single_parallel - Alpha - items: 10000  Time: 9897  Each: 0.9897
insert_single_parallel - Git - items: 10000  Time: 10143  Each: 1.0143
insert_single_bulk - Cwal - items: 10000  Time: 1862  Each: 0.1862
```

# Why?
This whole thing started as I was trying to migrate a script from node.js to rust but it was painfully slow.  
You can clearly see that the bulk insert on node.js makes a whole load of difference.  
A creiend recommended the mongodb_cwal create which is built off the old mongodb driver, preformance is close enough to the node.js version that I can consider using it.