use async_trait::async_trait;

use mongodb_stable;
use mongodb_git;
use mongodb_alpha;
use mongodb_cwal;


// going to use the bson from stable
pub use mongodb_stable::{bson:: {self, doc, document::Document}};

use serde::de::DeserializeOwned;


extern crate dotenv;
use dotenv::dotenv;
use std::env;

use futures::stream::{ StreamExt};
use std::error::Error;
use std::fmt::Debug;



#[async_trait]
pub trait DB {
    type Document;
    type FindOptions;
    type UpdateOptions;

    //db: Database;

    async fn new () -> Result<Self, Box<dyn Error>> where Self: Sized;

    async fn get_items<T: DeserializeOwned + Debug + std::marker::Send >(&self, collection: &str, filter: Self::Document, find_options: Option<Self::FindOptions>) -> Result<Vec<T>, Box<dyn Error>>;

    async fn set_item(&self, collection: &str, filter: Self::Document, data: Self::Document)  -> Result<(), Box<dyn Error>>;
}


#[derive(Clone, Debug)]
pub struct Stable {
    db: mongodb_stable::Database,
    database: String,
    client: mongodb_stable::Client
}

#[async_trait]
impl DB for Stable {
    type Document = mongodb_stable::bson::document::Document;
    type FindOptions = mongodb_stable::options::FindOptions;
    type UpdateOptions = mongodb_stable::options::UpdateOptions;

    async fn new() -> std::result::Result<Self, Box<dyn Error>> {
        dotenv().ok();

        let database = env::var("db").expect("db not set in .env");
        let location = env::var("dbLocation").expect("dbLocation not set in .env");
        let user = env::var("dbUser").expect("dbUser not set in .env");
        let pass = env::var("dbPass").expect("dbPass not set in .env");
        let url = format!("mongodb+srv://{}:{}@{}/admin?ssl=false", user, pass, location);


        let mut client_options = mongodb_stable::options::ClientOptions::parse_with_resolver_config(&url, mongodb_stable::options::ResolverConfig::cloudflare(), ).await?;

        // Manually set an option.
        client_options.app_name = Some("mongodb_rust_comparison".to_string());

        // Get a handle to the deployment.
        let client = mongodb_stable::Client::with_options(client_options)?;
        let db = client.database(&database);

        Ok(Self{ db, client, database })
    }

    async fn get_items<T: DeserializeOwned + Debug + std::marker::Send>(&self, collection: &str, filter: Self::Document, find_options: Option<Self::FindOptions>) -> Result<Vec<T>, Box<dyn Error>> {
        let mut cursor = self.db.collection(collection).find(filter, find_options).await?;

        let mut result:Vec<T> = Vec::new();
        while let Some(doc) = cursor.next().await {
            //println!("{:?}", &doc);
            match doc {
                Ok(doc_resolved)  => {
                    match bson::from_bson::<T>(doc_resolved.into()) {
                        Ok(document)  => {
                            result.push(document)
                        },
                        Err(_) => {},
                    };
                },
                Err(_) => {},
            }
        }

        Ok(result)
    }

    async fn set_item(&self, collection: &str, filter: Self::Document, data: Self::Document ) -> Result<(), Box<dyn Error>> {
        let options = Some(mongodb_stable::options::UpdateOptions::builder().upsert(Some(true)).build());
        self.db.collection(collection).update_one(filter, data, options).await.unwrap();
        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct Alpha {
    db: mongodb_alpha::Database,
    database: String,
    client: mongodb_alpha::Client
}

#[async_trait]
impl DB for Alpha {
    type Document = mongodb_alpha::bson::document::Document;
    type FindOptions = mongodb_alpha::options::FindOptions;
    type UpdateOptions = mongodb_alpha::options::UpdateOptions;

    async fn new() -> std::result::Result<Self, Box<dyn Error>> {
        dotenv().ok();

        let database = env::var("db").expect("db not set in .env");
        let location = env::var("dbLocation").expect("dbLocation not set in .env");
        let user = env::var("dbUser").expect("dbUser not set in .env");
        let pass = env::var("dbPass").expect("dbPass not set in .env");
        let url = format!("mongodb+srv://{}:{}@{}/admin?ssl=false", user, pass, location);


        let mut client_options = mongodb_alpha::options::ClientOptions::parse_with_resolver_config(&url, mongodb_alpha::options::ResolverConfig::cloudflare(), ).await?;

        // Manually set an option.
        client_options.app_name = Some("mongodb_rust_comparison".to_string());

        // Get a handle to the deployment.
        let client = mongodb_alpha::Client::with_options(client_options)?;
        let db = client.database(&database);

        Ok(Self{ db, client, database })
    }

    async fn get_items<T: DeserializeOwned + Debug + std::marker::Send>(&self, collection: &str, filter: Self::Document, find_options: Option<Self::FindOptions>) -> Result<Vec<T>, Box<dyn Error>> {
        let mut cursor = self.db.collection(collection).find(filter, find_options).await?;

        let mut result:Vec<T> = Vec::new();
        while let Some(doc) = cursor.next().await {
            //println!("{:?}", &doc);
            match doc {
                Ok(doc_resolved)  => {
                    match bson::from_bson::<T>(doc_resolved.into()) {
                        Ok(document)  => {
                            result.push(document)
                        },
                        Err(_) => {},
                    };
                },
                Err(_) => {},
            }
        }

        Ok(result)
    }

    async fn set_item(&self, collection: &str, filter: Self::Document, data: Self::Document ) -> Result<(), Box<dyn Error>> {
        let options = Some(mongodb_alpha::options::UpdateOptions::builder().upsert(Some(true)).build());
        self.db.collection(collection).update_one(filter, data, options).await.unwrap();
        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct Git {
    db: mongodb_git::Database,
    database: String,
    client: mongodb_git::Client
}

#[async_trait]
impl DB for Git {
    type Document = mongodb_git::bson::document::Document;
    type FindOptions = mongodb_git::options::FindOptions;
    type UpdateOptions = mongodb_git::options::UpdateOptions;

    async fn new() -> std::result::Result<Self, Box<dyn Error>> {
        dotenv().ok();

        let database = env::var("db").expect("db not set in .env");
        let location = env::var("dbLocation").expect("dbLocation not set in .env");
        let user = env::var("dbUser").expect("dbUser not set in .env");
        let pass = env::var("dbPass").expect("dbPass not set in .env");
        let url = format!("mongodb+srv://{}:{}@{}/admin?ssl=false", user, pass, location);


        let mut client_options = mongodb_git::options::ClientOptions::parse_with_resolver_config(&url, mongodb_git::options::ResolverConfig::cloudflare(), ).await?;

        // Manually set an option.
        client_options.app_name = Some("mongodb_rust_comparison".to_string());

        // Get a handle to the deployment.
        let client = mongodb_git::Client::with_options(client_options)?;
        let db = client.database(&database);

        Ok(Self{ db, client, database })
    }

    async fn get_items<T: DeserializeOwned + Debug + std::marker::Send>(&self, collection: &str, filter: Self::Document, find_options: Option<Self::FindOptions>) -> Result<Vec<T>, Box<dyn Error>> {
        let mut cursor = self.db.collection(collection).find(filter, find_options).await?;

        let mut result:Vec<T> = Vec::new();
        while let Some(doc) = cursor.next().await {
            //println!("{:?}", &doc);
            match doc {
                Ok(doc_resolved)  => {
                    match bson::from_bson::<T>(doc_resolved.into()) {
                        Ok(document)  => {
                            result.push(document)
                        },
                        Err(_) => {},
                    };
                },
                Err(_) => {},
            }
        }

        Ok(result)
    }

    async fn set_item(&self, collection: &str, filter: Self::Document, data: Self::Document ) -> Result<(), Box<dyn Error>> {
        let options = Some(mongodb_git::options::UpdateOptions::builder().upsert(Some(true)).build());
        self.db.collection(collection).update_one(filter, data, options).await.unwrap();
        Ok(())
    }
}


#[derive(Clone, Debug)]
pub struct Cwal {
    db: mongodb_cwal::db::Database,
    database: String,
    client: mongodb_cwal::Client
}


impl Cwal {
    pub fn new() -> Result<Self, Box<dyn Error>> {
        use mongodb_cwal::{Client, ThreadedClient};
       // use mongodb_cwal::db::ThreadedDatabase;
        dotenv().ok();

        let database = env::var("db").expect("db not set in .env");
        let location = env::var("dbLocation").expect("dbLocation not set in .env");
        let user = env::var("dbUser").expect("dbUser not set in .env");
        let pass = env::var("dbPass").expect("dbPass not set in .env");
        let url = format!("mongodb+srv://{}:{}@{}/admin?ssl=false", user, pass, location);

        // Manually set an option.
        //client_options.app_name = Some("mongodb_rust_comparison".to_string());

        // Get a handle to the deployment.
        let client = Client::with_uri(&url).expect("Failed to initialize client.");
        let db = client.db(&database);

        Ok(Self{ db, client, database })
    }

    #[allow(dead_code)]
    pub fn get_items<T: DeserializeOwned + Debug + std::marker::Send>(&self, collection: &str, filter: Option<mongodb_cwal::Document>, find_options: Option<mongodb_cwal::coll::options::FindOptions>) -> Result<Vec<T>, Box<dyn Error>> {
        use mongodb_cwal::db::ThreadedDatabase;
        use mongodb_cwal::{from_bson};
        let mut cursor = self.db.collection(collection).find(filter, find_options).ok().expect("Failed to execute find.");

        let mut result:Vec<T> = Vec::new();


        while let Some(doc) = cursor.next() {
            //println!("{:?}", &doc);
            match doc {
                Ok(doc_resolved)  => {
                    match from_bson::<T>(doc_resolved.into()) {
                        Ok(document)  => {
                            result.push(document)
                        },
                        Err(_) => {},
                    };
                },
                Err(_) => {},
            }
        }

        /*

        for doc in cursor {
            if let Ok(doc_resolved) = doc {
                match bson::from_bson::<T>(doc_resolved.into()) {
                    Ok(document)  => {
                        result.push(document)
                    },
                    Err(_) => {},
                };
            }
        }

         */


        /*

        while let Some(doc) = cursor.next() {
            //println!("{:?}", &doc);
            match doc {
                Ok(doc_resolved)  => {
                    match bson::from_bson::<T>(doc_resolved.into()) {
                        Ok(document)  => {
                            result.push(document)
                        },
                        Err(_) => {},
                    };
                },
                Err(_) => {},
            }
        }

         */

        Ok(result)
    }

    #[allow(dead_code)]
    pub fn set_item(&self, collection: &str, filter: mongodb_cwal::Document, data: mongodb_cwal::Document ) -> Result<(), Box<dyn Error>> {
        use mongodb_cwal::db::ThreadedDatabase;

        let options = mongodb_cwal::coll::options::UpdateOptions{ upsert: Some(true), write_concern: None };

        self.db.collection(collection).update_one(filter, data, Some(options)).unwrap();
        Ok(())
    }

    pub fn set_item_bulk(&self, collection: &str, items: Vec<mongodb_cwal::coll::options::WriteModel>, ordered: bool ) -> Result<(), Box<dyn Error>> {
        use mongodb_cwal::db::ThreadedDatabase;

        self.db.collection(collection).bulk_write(items, ordered);
        Ok(())
    }
}
