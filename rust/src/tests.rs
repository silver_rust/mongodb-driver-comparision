use serde::{Deserialize, Serialize};
use std::time::SystemTime;

use crate::db::{Stable,Alpha, Git, DB, bson, doc, Document, Cwal};
use mongodb_cwal;

use futures::stream::FuturesUnordered;
use futures::{TryStreamExt};

#[derive(Serialize, Deserialize, Debug)]
pub struct TestObject {
    #[serde(skip_serializing_if = "Option::is_none")]
    _id: Option<bson::oid::ObjectId>,

    id: i32,
    field1: String,
    field2: String
}
pub trait ForInsert {
    fn process_for_update(&self) -> Document;

    fn process_for_update_cwal(&self) -> mongodb_cwal::coll::options::WriteModel;
}
impl ForInsert for TestObject {
    fn process_for_update(&self) -> Document {
        bson::to_bson(&self).unwrap().as_document().unwrap().to_owned()
    }
    fn process_for_update_cwal(&self) -> mongodb_cwal::coll::options::WriteModel {
        mongodb_cwal::coll::options::WriteModel::UpdateOne {
            filter: mongodb_cwal::doc!{ "id": self.id },
            update: mongodb_cwal::to_bson(&self).unwrap().as_document().unwrap().to_owned(),
            upsert: Some(true),
        }
    }
}


fn generate_array() -> Vec<TestObject>{
    let mut data:Vec<TestObject> = Vec::new();

    while data.len() < 10000 {
        data.push(TestObject{
            _id: None,
            id: data.len() as i32,
            field1: "rust".to_string(),
            field2: "".to_string()
        })
    }

    data
}


pub async fn insert_single_linear(){
    insert_single_linear_stable().await;
    insert_single_linear_alpha().await;
    insert_single_linear_git().await;
}

pub async fn insert_single_linear_stable(){
    let items = generate_array();
    let length = items.len() as f64;
    let database_stable = Stable::new().await.unwrap();

    let now = SystemTime::now();
    for item in &items {
        database_stable.set_item("test", doc!{"id":item.id}, item.process_for_update() ).await.unwrap();
    }
    let elapsed = match now.elapsed() { Ok(elapsed) => { elapsed.as_millis() }, Err(_) => { 0 } } as f64;

    println!("insert_single_linear - Stable - items: {}  Time: {}  Each: {}", &items.len(), &elapsed, &elapsed/&length );
}

pub async fn insert_single_linear_alpha(){
    let items = generate_array();
    let length = items.len() as f64;
    let database_alpha = Alpha::new().await.unwrap();

    let now = SystemTime::now();
    for item in &items {
        database_alpha.set_item("test", doc!{"id":item.id}, item.process_for_update() ).await.unwrap();
    }
    let elapsed = match now.elapsed() { Ok(elapsed) => { elapsed.as_millis() }, Err(_) => { 0 } } as f64;

    println!("insert_single_linear - Alpha - items: {}  Time: {}  Each: {}", &items.len(), &elapsed, &elapsed/&length );
}

pub async fn insert_single_linear_git(){
    let items = generate_array();
    let length = items.len() as f64;
    let database_git = Git::new().await.unwrap();

    let now = SystemTime::now();
    for item in &items {
        database_git.set_item("test", doc!{"id":item.id}, item.process_for_update() ).await.unwrap();
    }
    let elapsed = match now.elapsed() { Ok(elapsed) => { elapsed.as_millis() }, Err(_) => { 0 } } as f64;

    println!("insert_single_linear - Git - items: {}  Time: {}  Each: {}", &items.len(), &elapsed, &elapsed/&length );
}


pub async fn insert_single_parallel(){
    insert_single_parallel_stable().await;
    insert_single_parallel_alpha().await;
    insert_single_parallel_git().await;
}

async fn insert_single_parallel_stable(){
    let items = generate_array();
    let length = items.len() as f64;
    let database = Stable::new().await.unwrap();

    let now = SystemTime::now();

    let set_item_futures= FuturesUnordered::new();
    for item in &items {
        set_item_futures.push(database.set_item("test", doc!{"id":item.id}, item.process_for_update() ))
    }

    set_item_futures.try_for_each_concurrent(25000, |_| async move { Ok(()) }).await.unwrap();

    let elapsed = match now.elapsed() { Ok(elapsed) => { elapsed.as_millis() }, Err(_) => { 0 } } as f64;

    println!("insert_single_parallel - Stable - items: {}  Time: {}  Each: {}", &items.len(), &elapsed, &elapsed/&length );
}

async fn insert_single_parallel_alpha(){
    let items = generate_array();
    let length = items.len() as f64;
    let database = Alpha::new().await.unwrap();

    let now = SystemTime::now();

    let set_item_futures= FuturesUnordered::new();
    for item in &items {
        set_item_futures.push(database.set_item("test", doc!{"id":item.id}, item.process_for_update() ))
    }

    set_item_futures.try_for_each_concurrent(25000, |_| async move { Ok(()) }).await.unwrap();

    let elapsed = match now.elapsed() { Ok(elapsed) => { elapsed.as_millis() }, Err(_) => { 0 } } as f64;

    println!("insert_single_parallel - Alpha - items: {}  Time: {}  Each: {}", &items.len(), &elapsed, &elapsed/&length );
}

async fn insert_single_parallel_git(){
    let items = generate_array();
    let length = items.len() as f64;
    let database = Git::new().await.unwrap();

    let now = SystemTime::now();

    let set_item_futures= FuturesUnordered::new();
    for item in &items {
        set_item_futures.push(database.set_item("test", doc!{"id":item.id}, item.process_for_update() ))
    }

    set_item_futures.try_for_each_concurrent(25000, |_| async move { Ok(()) }).await.unwrap();

    let elapsed = match now.elapsed() { Ok(elapsed) => { elapsed.as_millis() }, Err(_) => { 0 } } as f64;

    println!("insert_single_parallel - Git - items: {}  Time: {}  Each: {}", &items.len(), &elapsed, &elapsed/&length );
}


pub fn insert_single_bulk(){
    insert_single_bulk_cwal();
}

fn insert_single_bulk_cwal(){
    let items = generate_array();
    let length = items.len() as f64;

    let database = Cwal::new().unwrap();

    let now = SystemTime::now();
    let mut bulk_items= Vec::new();
    for item in &items {
        bulk_items.push(item.process_for_update_cwal())
    }
    database.set_item_bulk("test", bulk_items, false).unwrap();


    let elapsed = match now.elapsed() { Ok(elapsed) => { elapsed.as_millis() }, Err(_) => { 0 } } as f64;

    println!("insert_single_bulk - Cwal - items: {}  Time: {}  Each: {}", &items.len(), &elapsed, &elapsed/&length );
}



