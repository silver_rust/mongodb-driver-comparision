use async_std::task;
mod db;
mod tests;

use tests::{insert_single_linear, insert_single_parallel, insert_single_bulk};




fn main(){
    task::block_on(main_async());
}

async fn main_async(){
    println!("Rust");
    println!("Times in ms");


    insert_single_linear().await;
    insert_single_parallel().await;
    insert_single_bulk();
}